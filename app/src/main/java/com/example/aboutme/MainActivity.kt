package com.example.aboutme

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    //private lateinit val inputMethodManager: InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        // Inflate the screen
        super.onCreate(savedInstanceState)

        // Bind the main activity layout
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        // When the done button is pressed
        binding.doneButton.setOnClickListener{
            // Update Nickname
            updateNickname()
        }

    }

    // Get nickname and update it on screen
    private fun updateNickname() {
        // Apply the binding to all elements
        binding.apply {

            // Update nickname text view
            nicknameTextView.text = getString(R.string.your_nickname) + " " + nicknameEditText.text + ""

            // Finally show nickname text view and hide done button and nickname text edit
            invalidateAll()
            doneButton.visibility = View.GONE
            nicknameEditText.visibility = View.GONE
            nicknameTextView.visibility = View.VISIBLE
        }

        // Hide the keyboard
        /*val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowsToken, 0)*/
    }
}
