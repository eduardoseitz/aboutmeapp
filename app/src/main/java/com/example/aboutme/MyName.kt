package com.example.aboutme

// My name data binding class
data class MyName(
    var name: String,
    var nickname: String
)
